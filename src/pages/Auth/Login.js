import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col, Alert } from 'react-bootstrap';
import { Navigate, useNavigate, Link } from 'react-router-dom';

// import Swal from 'sweetalert2';
import Banner from '../../components/Banner';
import UserContext from '../../UserContext';


function Login() {

    const {setUser} = useContext(UserContext);
    const navigate = useNavigate();


    let credInit = { email: '', password: '' };
    const [cred, setCred] = useState(credInit)
    const [isActive, setIsActive] = useState(false)
    const [isFailed, setIsFailed] = useState(false)
    const [isRegister, setIsRegister] = useState(false)
    const [btnLabel, setBtnLabel] = useState('Login')
    const [isEmailExists, setIsEmailExists] = useState(false)

    function getChanges(field) {
        return (e) => setCred((cred) => ({ ...cred, [field]: e.target.value }));
    }

  

    function Message() {
        if (isRegister) {
            return (
                 <Alert variant='light' className='small'>
                 Already have an account?
                    <Alert.Link as={Link} onClick={() => setIsRegister(false)} className="text-primary"> Login</Alert.Link> instead.
                </Alert>
            )
        } else {
            return (
                <Alert variant='light' className='small'>
                    Don't have an account? 
                    <Alert.Link as={Link} onClick={() => setIsRegister(true)} className="text-primary"> Register</Alert.Link> here.
                </Alert>
            )
        }
    }

    useEffect(() => {
        if (cred.email !== '' && cred.password !== '') {
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [cred, isActive, isFailed])

    useEffect(() => {
        setIsFailed(false)
        setIsEmailExists(false)
        if(isRegister){
            setBtnLabel('Next')
        }else{
            setBtnLabel('Login')
        }
    },[isRegister,btnLabel])

    const urlPath = process.env.REACT_APP_API_URL;

    function login(e) {
        e.preventDefault();

        if(isRegister){
            checkEmail();
            console.log('IsRegister: ' + isRegister)
        }else{

            let url = `${urlPath}/users/login`
            fetch(url, {
                method: 'POST',
                headers: {
                    'Content-Type':'application/json'
                },
                body: JSON.stringify(cred)
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                if(typeof data.access !== 'undefined'){
                    localStorage.setItem('token',data.access)
                    retrieveUserDetails(data.access)
                    navigate('/')

                }else{
                    setCred(credInit)
                    setIsActive(false)
                    setIsFailed(true)
                }

            })

            setCred(credInit)

        }

    }

    const retrieveUserDetails = (token) => {
        let url = `${urlPath}/users/details`;

        fetch(url, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            setUser(data)
        })
    }

    function checkEmail(){
        let url = `${urlPath}/users/checkEmail`;
        fetch(url,{
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({email: cred.email})
            }).then(res => res.json())
            .then(data => {
                console.log(data)
                if(!data){
                    navigate('/register',{
                        state: {
                            email: cred.email
                        }
                    })
                }else{
                    setIsEmailExists(true)
                }
            })


    }



    return (

        <Row>
            <Col xs={{span: 12, order: 'last'}} md={{span: 6, order: 'first'}}>
                <Banner />
            </Col>
            <Col className='my-auto' xs={{span: 12, order: 'first'}} md={{span: 6, order: 'last'}}>
                { !isRegister ? <h1>Login</h1> : <h1>Sign Up</h1> }

                { isFailed &&
                    <Alert variant='danger' className='small p-1'>Authentication failed. Please check your credentials then try again.
                    </Alert>
                }

                { isEmailExists &&
                    <Alert variant='danger' className='small p-1'>Email already exists.
                    </Alert>
                }
                
                <Form onSubmit={(e) => login(e)} className="mt-2">
                    <Form.Group className="mb-3" controlId="email">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control
                            type="email"
                            value={cred.email}
                            onChange={getChanges('email')}
                            placeholder="johndoe@email.com" />
                    </Form.Group>
                   { !isRegister && 
                    <Form.Group className="mb-3" controlId="password">
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                            type="password"
                            value={cred.password}
                            onChange={getChanges('password')}
                            placeholder="******" />
                    </Form.Group>
                    }

                    <Row>
                        <Col xs={12} md={4}>
                            <Button variant="primary" type="submit" id="submitBtn">{btnLabel}</Button>
                        </Col>
                        <Col xs={12} md={8}>
                            <Message />
                        </Col>

                    </Row>

                </Form>
               


            </Col>
        </Row>

    )

}

export default Login;