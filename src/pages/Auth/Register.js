import { useState, useEffect } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { Form, FloatingLabel, Button, Container, Row, Col } from 'react-bootstrap'

import Banner from '../../components/Banner';


function Register() {

    // const navigate = useNavigate();
    const location = useLocation();
    const navigate = useNavigate();
    let propsEmail = location.state == null ? '' : location.state.email;

    let regInit = {
        firstName: '',
        lastName: '',
        mobileNo: '',
        email: propsEmail,
        address: '',
        password: '',
        password2: '',
    }

    const [regUser, setRegUser] = useState(regInit);
    const [isActive, setIsActive] = useState(false);
    

    function registerUser(e) {
        e.preventDefault();
        const urlPath = process.env.REACT_APP_API_URL;
        const url = `${urlPath}/users/register`

        fetch(url,{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(regUser)
        }).then(res => res.json())
        .then(data => {
            console.log(data)

            setRegUser(regInit)
            navigate('/login')

        })


    }

    useEffect(() => {

        if ((regUser.firstName !== '' && regUser.lastName !== '' && regUser.email !== '' && regUser.email.includes('@') && regUser.mobileNo.length === 11) && (regUser.password === regUser.password2)) {
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [regUser])


    function getChanges(field) {
        return (e) => setRegUser((regUser) => ({ ...regUser, [field]: e.target.value }));
    }
    return (
        <Row >
            <Col xs={{span: 12, order: 'last'}} md={{span: 6, order: 'first'}} className='justify-content-md-center'>
                <Banner />
            </Col>
            <Col xs={{span: 12, order: 'first'}} md={{span: 6, order: 'last'}}>
                 <Container className='bg-dark p-5'>

            <h2 className='text-white'>Create Account</h2>
            <Form onSubmit={(e) => registerUser(e)}>
                <FloatingLabel
                    controlId="firstName"
                    label="First Name"
                    className="mb-3"
                >
                    <Form.Control
                        type="text"
                        value={regUser.firstName}
                        onChange={getChanges('firstName')}
                        placeholder="First Name" />
                </FloatingLabel>

                <FloatingLabel
                    controlId="lastName"
                    label="Last Name"
                    className="mb-3"
                >
                     <Form.Control
                        type="text"
                        value={regUser.lastName}
                        onChange={getChanges('lastName')}
                        placeholder="Last Name" />
                </FloatingLabel>

                <FloatingLabel
                    controlId="userEmail"
                    label="Email address"
                    className="mb-3"
                >
                     <Form.Control
                        type="email"
                        value={regUser.email}
                        onChange={getChanges('email')}
                        placeholder="johndoe@email.com" />
                    <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
                    </Form.Text>
                </FloatingLabel>
                <FloatingLabel
                    controlId="mobileNo"
                    label="Mobile No"
                    className="mb-3"
                >
                     <Form.Control
                        type="text"
                        value={regUser.mobileNo}
                        onChange={getChanges('mobileNo')}
                        placeholder="+63-" />
                </FloatingLabel>
                <FloatingLabel
                    controlId="address"
                    label="Address"
                    className="mb-3"
                >
                     <Form.Control
                        type="text"
                        value={regUser.address}
                        onChange={getChanges('address')}
                         />
                </FloatingLabel>
                <FloatingLabel controlId="password" label="Password" className='mb-3'>
                    <Form.Control
                            type="password"
                            value={regUser.password}
                            onChange={getChanges('password')}
                            placeholder="Enter Password" />
                </FloatingLabel>
                <FloatingLabel controlId="password2" label="Confirm Password" className='mb-3'>
                <Form.Control
                        type="password"
                        value={regUser.password2}
                        onChange={getChanges('password2')}
                        placeholder="Confirm Password" />
                </FloatingLabel>
                {isActive ?
                    <Button variant="primary" type="submit" id='submitBtn'>
                        Submit
                    </Button>
                    :
                    <Button variant="primary" type="submit" id='submitBtn' disabled>
                        Submit
                    </Button>
                }
            </Form>
        </Container>
            </Col>
        </Row>



       
    )
}


export default Register;