import { useContext, useEffect } from "react";
import { Navigate } from "react-router-dom";
import UserContext from "../../UserContext";
import CartContext from "../../CartContext";
import WishContext from "../../WishContext";

function Logout(){

    const {unsetUser, setUser} = useContext(UserContext)
    const {setCart} = useContext(CartContext)
    const {setWish} = useContext(WishContext)

    unsetUser();
    useEffect(() => {
        setUser({})
        setCart([])
        setWish([])
    },[setUser,setCart])

    return(
        <Navigate to='/'/>
    )
}

export default Logout;