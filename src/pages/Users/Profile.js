import { useContext, useEffect, useState } from 'react';
import { Accordion, Card, Form, FloatingLabel, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../../UserContext'


function Profile() {

    const { user } = useContext(UserContext)
    const [oldPassword, setOldPassword] = useState('')
    const [password, setPassword] = useState('')
    const [password1, setPassword1] = useState('')
    const [isActive, setIsActive] = useState('disabled')

    useEffect(() => {
        if (password === password1 && oldPassword !== '') {
            setIsActive('')
        } else {
            setIsActive('disabled')
        }


    }, [password, password1, oldPassword])

    function getChanges(field) {
        if (field === 'password') {
            return (e) => {
                setPassword(e.target.value)
            };
        } else if(field === 'oldPassword') {
            return (e) => {
                setOldPassword(e.target.value)
            };
        }else {
            return (e) => {
                setPassword1(e.target.value)
            };
        }
    }

    function changePassword(e) {
        e.preventDefault();
        const urlPath = process.env.REACT_APP_API_URL;
        const url = `${urlPath}/users/changepassword`

        Swal.fire({
            title: "Please wait...",
            didOpen: () => Swal.showLoading()
        })

        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                oldPassword: oldPassword,
                newPassword: password
            })
        }).then(res => res.json())
            .then(data => {
                // console.log(data)
                if (data) {
                    Swal.fire({
                        title:'Success',
                        text: 'Password changed.',
                        icon: 'success'
                    })
                } else {
                    Swal.fire({
                        title:'Failed',
                        text: 'Password not changed.',
                        icon: 'error'
                    })
                }

                setPassword('')
                setOldPassword('')
                setPassword1('')

            })




    }


    return (
        <>
            <Card>
                <Card.Header>User Details</Card.Header>
                <Card.Body>
                    <p><strong>UserId: </strong>{user._id}</p>
                    <p><strong>Name: </strong>{user.firstName + ' ' + user.lastName}</p>
                    <p><strong>Email: </strong>{user.email}</p>
                    <p><strong>Mobile: </strong>{user.mobileNo}</p>
                    <p><strong>Address: </strong>{user.address}</p>
                </Card.Body>
            </Card>
            <Accordion className='mt-5'>
                <Accordion.Item eventKey='0'>
                    <Accordion.Header>Change Password</Accordion.Header>
                    <Accordion.Body>
                        <Form onSubmit={(e) => changePassword(e)}>
                            <FloatingLabel controlId="oldPassword" label="Old Password" className='mb-3'>
                                <Form.Control
                                    type="password"
                                    value={oldPassword}
                                    onChange={getChanges('oldPassword')}
                                    />
                            </FloatingLabel>
                            <FloatingLabel controlId="password" label="Password" className='mb-3'>
                                <Form.Control
                                    type="password"
                                    value={password}
                                    onChange={getChanges('password')}
                                     />
                            </FloatingLabel>
                            <FloatingLabel controlId="password1" label="Confirm Password" className='mb-3'>
                                <Form.Control
                                    type="password"
                                    value={password1}
                                    onChange={getChanges('password2')}
                                     />
                            </FloatingLabel>
                            <Form.Group>
                                <Button variant="primary" type="submit" id='submitBtn' disabled={isActive}>
                                    Update
                                </Button>
                            </Form.Group>
                        </Form>
                    </Accordion.Body>
                </Accordion.Item>
            </Accordion>

        </>
    )
}

export default Profile;