import { useEffect, useState } from "react";
import { Form, Table } from "react-bootstrap";
import Swal from "sweetalert2";


function List() {

    const [users, setUsers] = useState([])

    useEffect(() => {

        fetch(`${process.env.REACT_APP_API_URL}/users`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        }).then(res => res.json())
            .then(data => {
                // console.log(data)
                setUsers(data)
            })


    }, [])

    function setAdmin(userId,isAdmin){

        let level = JSON.parse(isAdmin) ? 'Admin' : 'User';
        

        Swal.fire({
            title: 'Confirm?',
            text: `Are you sure you want to change user role to ${level}?`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
          }).then((result) => {

            if (result.isConfirmed) {
                fetch(`${process.env.REACT_APP_API_URL}/users/setadmin`, {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${localStorage.getItem('token')}`
                    },
                    body: JSON.stringify({
                        userId: userId,
                        isAdmin: JSON.parse(isAdmin)
                    })
                }).then(res => res.json())
                .then(data => {
                    // console.log(data)
                    if(data){
                        Swal.fire({
                            title:'Success',
                            text: 'Role changed.',
                            icon: 'success'
                        })
                    }else{
                        Swal.fire({
                            title:'Failed',
                            text: 'Role not changed.',
                            icon: 'error'
                        })
                    }
                    // setUsers(data)
                })

            }

          })
    }



    return (
        <Table striped responsive>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Is Admin</th>
                </tr>
            </thead>
            <tbody>
                {
                    users.map((user) => {
                        return (
                            <tr key={user._id}>
                                <td>{user.firstName + ' ' + user.lastName}</td>
                                <td>{user.email}</td>
                                <td>
                                    <Form>
                                        <Form.Select size='sm' 
                                            defaultValue={user.isAdmin} 
                                            onChange={(e) => {setAdmin(user._id,e.target.value)}}
                                        >
                                            <option value='true'>Admin</option>
                                            <option value='false'>User</option>
                                        </Form.Select>
                                    </Form>
                                </td>
                            </tr>
                        )
                    })
                }
            </tbody>

        </Table>
    )
}

export default List;