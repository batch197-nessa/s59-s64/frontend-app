import { useContext, useEffect, useState } from "react"
import { InputGroup, Form, Button } from "react-bootstrap"

import CartContext from "../../../CartContext"
import './Item.css'

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrashCan } from '@fortawesome/free-solid-svg-icons'

function Item({ item }) {
    // console.log('item')
    // console.log(item)

    const { productName, quantity, price, productId } = item
    const [qty, setQty] = useState(quantity)
    const { cart, setCart } = useContext(CartContext)


    const total = parseInt(qty * price)

    const alignRight = {
        textAlign: 'right'
    }

    const idx = cart.findIndex((cartItem) => cartItem.productId === productId)
    function setItemQty() {
        return (e) => setQty(e.target.value)
    }

    useEffect(() => {
        setCart((cart) => {
            const arr = [...cart]
            arr[idx].quantity = parseInt(qty);
            return arr
        })

    }, [qty])


    function deleteItem(){

        setCart((cart) => cart.filter((obj) => {
            return obj.productId !== productId
        }))

    }

    return (
        <tr>
            <td>
                {productName}
                <span style={{ float: 'right' }}>
                    <Button variant="light" 
                        style={{backgroundColor: 'transparent',borderColor: 'transparent', color: '#00000040'}}
                        onClick={() => deleteItem()}
                        >
                        <FontAwesomeIcon icon={faTrashCan}/>
                    </Button>
                </span>
            </td>
            {/* <td>{item.quantity}</td> */}
            <td>
                <InputGroup className="">
                    <Button variant="outline-secondary" size="sm"
                        onClick={() => {
                            let el = document.getElementById(`ctrlQty${idx}`)
                            if (el.value == 1) {
                                el.value = 1
                            } else {
                                el.value = parseInt(el.value) - 1
                            }
                            setQty(el.value)
                        }}
                    >-</Button>
                    <Form.Control
                        type='number'
                        value={qty}
                        onChange={setItemQty()}
                        id={`ctrlQty${idx}`}
                        min={1}
                    />
                    <Button variant="outline-secondary" size="sm"
                        onClick={() => {
                            let el = document.getElementById(`ctrlQty${idx}`)
                            el.value = parseInt(el.value) + 1
                            setQty(el.value)
                        }}
                    >+</Button>
                </InputGroup>
            </td>
            <td>{price}</td>
            <td style={alignRight}>{total}</td>
        </tr>
    )
}


export default Item