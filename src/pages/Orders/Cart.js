import { useContext } from "react";
import { Table, Button, Form, Row, Col, Stack } from "react-bootstrap";
import Item from "./components/Item";
import CartContext from "../../CartContext";
import UserContext from "../../UserContext";
import Swal from "sweetalert2";

function Cart() {

    const urlPath = process.env.REACT_APP_API_URL;
    const token = localStorage.getItem('token');

    const { cart, setCart } = useContext(CartContext)
    const { user } = useContext(UserContext)
    let sum = 0;


    const alignRight = {
        textAlign: 'right'
    }


    const items = cart.map((item, index) => {
        let total = item.price * item.quantity;
        sum = sum + total;

        return <Item item={item} key={index} />

    })


    function checkOut() {
        let el = document.getElementById('shipTo')
        let val = el.value

        if (val === '') {
            Swal.fire({
                title: 'Notice!',
                text: 'Please provide Shipping Address',
                icon: 'warning'
            })
        } else {
            let products = [];

            for(let item of cart){
                products.push({
                    productId: item.productId,
                    productName: item.productName,
                    price: item.price,
                    quantity: item.quantity,
                    totalAmount: parseInt(item.price) * parseInt(item.quantity)

                })
            }

           
            let data = {
                products: products,
                totalAmount: sum,
                shippingAddress: val
            }

            fetch(`${urlPath}/orders`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`
                },
                body: JSON.stringify(data)
            })
            .then(res => res.json())
            .then(result => {
                console.log('result: ' + result)
                if (result) {
                    setCart([])
                    Swal.fire({
                        title: 'Success!',
                        text: 'Order created successfully.',
                        icon: "success"
                    })
                }else{
                    Swal.fire({
                        title: 'Error!',
                        text: 'Something went wrong.',
                        icon: 'error'
                    })
                }
            })
        }







    }

    return (
        <>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>Product Name</th>
                        <th style={{ width: '15%' }}>Qty</th>
                        <th>Price</th>
                        <th style={alignRight}>Total Amount</th>
                    </tr>
                </thead>
                <tbody>
                    {items}
                </tbody>
                <tfoot>
                    <tr>
                        <th colSpan={3} style={alignRight}>OverAll: </th>
                        <th style={alignRight}>&#8369; {sum}</th>
                    </tr>
                </tfoot>
            </Table>

            <Stack direction="horizontal" gap={3}>
                <Form.Group as={Row} className="me-auto w-100">
                    <Form.Label column sm={3}>Shipping Address: </Form.Label>
                    <Col sm={9}>
                        <Form.Control
                            placeholder="Shipping Address:"
                            defaultValue={user.address}
                            id='shipTo'
                        />
                    </Col>
                </Form.Group>
                <Button variant="danger" style={{ float: 'right' }} onClick={() => checkOut()} disabled={sum === 0}>Checkout</Button>

            </Stack>

        </>
    )
}

export default Cart;