import { useEffect, useState } from "react";
import { Accordion, Table } from "react-bootstrap";


function Orders() {

    const [orders, setOrders] = useState([])

    const headerStyle = {
        fontWeight: 'bold',
        fontSize: '.875em'
    }

    useEffect(() => {

        fetch(`${process.env.REACT_APP_API_URL}/orders`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(res => res.json())
            .then(data => {

                let sorted = data.sort((obj1, obj2) => {
                    let date1 = new Date(obj1.purchasedOn)
                    let date2 = new Date(obj2.purchasedOn)
                    return Number(date2) - Number(date1)

                })
                setOrders(sorted)
            })


    }, [])




    return (
        <Accordion>
            {orders.length > 0 ?

                orders.map((order, index) => {
                    let purDateProp = new Date(order.purchasedOn)
                    let purDate = `
                    ${purDateProp.getFullYear()}-${purDateProp.getMonth() + 1}-${purDateProp.getDate()} 
                    `

                    let productList = order.products.map(p => {
                        return (
                            <tr key={p.productId}>
                                <td>{p.productName}</td>
                                <td style={{width:'10%'}}>{p.quantity}</td>
                                <td>&#8369; {p.price}</td>
                                <td style={{textAlign: 'right'}}>&#8369; {p.totalAmount}</td>
                            </tr>
                        )
                    })
                    return (
                        <Accordion.Item eventKey={index} key={index}>
                            <Accordion.Header>
                                <div style={headerStyle}>OrderId: {order._id} | Date Purchased: {purDate}</div></Accordion.Header>
                            <Accordion.Body className="small mb-4">
                                <p><strong>UserID: </strong> {order.userId}</p>
                                <p><strong>Shipped To: </strong> {order.shippingAddress}</p>
                                <Table responsive bordered>
                                    <tbody>{productList}</tbody>
                                </Table>
                                <p style={{float: 'right',fontWeight: 'bold'}}>&#8369; {order.totalAmount}</p>

                            </Accordion.Body>
                        </Accordion.Item>
                    )


                })



                :
                <p>No orders yet. Add some products to your cart.</p>
            }
        </Accordion>
    )

}

export default Orders;