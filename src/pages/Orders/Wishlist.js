import { useContext, useEffect, useState } from "react"
import { Card, ListGroup, Table } from "react-bootstrap"

import WishContext from "../../WishContext"
import CartButtons from "../Products/components/CartButtons";



function Wishlist() {

    const { wish } = useContext(WishContext);
    const [wishlist, setWishlist] = useState(wish)


    // useEffect(() => {

    //     setWishlist((wl) => 

    //    )

    // }, [wish])

    return (
        <Card style={{  }}>
            <Card.Header><strong>Wish List</strong></Card.Header>
            <Card.Body>
                <Table>
                    {
                        wish.map((w, idx) => {
                            w.name = w.productName;
                            return (
                                <tr key={idx}>
                                    <td>{w.productName}</td>
                                    <td><CartButtons cartProp={w} /></td>
                                </tr>
                            )

                        })
                    }

                </Table>
            </Card.Body>
        </Card>



    )


}

export default Wishlist