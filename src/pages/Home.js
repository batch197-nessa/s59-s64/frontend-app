import AppCarousel from '../components/Carousel';
import Featured from '../components/Featured';

function Home() {

    return (
        <>
           
            <AppCarousel />
            <h1 className='mt-5'>Featured</h1>
            <Featured />
            
        </>
    )


}

export default Home;