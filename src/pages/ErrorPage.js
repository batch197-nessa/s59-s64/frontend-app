import { Button, Container } from "react-bootstrap"
import { Link } from "react-router-dom"

export default function ErrorPage() {

    return (
        <Container className="text-center" style={{ minHeight: '100%' }}>
            <img src="/images/error.png"
                width='200'
                height='200'
                alt="error_img"

            />

            <h3 className="mt-5">
                Feeling lost? Time to go
                <Button variant="link" as={Link} to='/' style={{fontSize: '1.75rem'}}>home.</Button>
            </h3>

        </Container>
    )
}