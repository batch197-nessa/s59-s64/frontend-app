import { useContext } from "react";
import { Navigate} from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../../UserContext";

import ProductForm from "./components/ProductForm";

function ProductCreate() {

    const urlPath = process.env.REACT_APP_API_URL;
    let token = localStorage.getItem('token');
    const { user } = useContext(UserContext)

    let product = { name: '', description: '', price: 0, isActive: true };

    const getFormData = (data) => {
        // console.log('data from form:')
        // console.log(data)

        let url = `${urlPath}/products`

        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${token}`
            },
            body: JSON.stringify({
                name: data.name,
                description: data.description,
                price: data.price
            })
        })
            .then(res => res.json())
            .then(data => {
                // console.log(data)
                if (data) {
                   
                    Swal.fire({
                        title: 'Success!',
                        text: 'Product added successfully',
                        icon: 'success'
                    })

                } else {
                    Swal.fire({
                        title: 'Error!',
                        text: 'Add Product failed',
                        icon: 'error',
                    })
                }

            })




    }
    product.onSubmit = getFormData;
    product.mode = 'Add'
    product.submitLbl = 'Submit'





    return (

        user.isAdmin ?

                <ProductForm productProp={product} />
            :

            <Navigate to='/' />

    )

}

export default ProductCreate