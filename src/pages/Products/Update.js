import { useContext } from "react";
import { Navigate, useLocation } from "react-router-dom";
import Swal from "sweetalert2";
import ProductForm from "./components/ProductForm";
import UserContext from "../../UserContext";

function ProductUpdate() {

    const urlPath = process.env.REACT_APP_API_URL;
    let token = localStorage.getItem('token');
    const { user } = useContext(UserContext)

    // //FOR EDIT
    const location = useLocation();
    let product = location.state.product;
   

    const getFormData = (data) => {
        // console.log('data from form:')
        // console.log(data)

        let url = `${urlPath}/products/${data._id}/update`

        fetch(url,{
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${token}`
            },
            body: JSON.stringify({
                name: data.name,
                description: data.description,
                price: data.price,
                isActive:data.isActive
            })
        })
        .then(res => res.json())
        .then(result => {
            // console.log(result)
            if(result){
                Swal.fire({
                    title: 'Success!',
                    text: 'Product updated successfully',
                    icon: 'success'
                })
            }else{
                Swal.fire({
                    title: 'Error!',
                    text: 'Update Product failed',
                    icon: 'error',
                })
            }
        })



        
    }
    product.onSubmit = getFormData;
    product.mode = 'Edit'
    product.submitLbl = 'Update'


    return (
        user.isAdmin ? 
            <ProductForm productProp={product}/>
          
        :
            <Navigate to='/' />

    )

}

export default ProductUpdate