import { Row, Col } from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import { useLocation } from "react-router-dom";

import ProductCard from "./components/ProductCard";
import UserContext from "../../UserContext";


function ViewAll(){

    const {user} = useContext(UserContext)
    const [products, setProducts] = useState([]);
    const location = useLocation();
   

    useEffect(() => {
        const urlPath = process.env.REACT_APP_API_URL;
        const url = user.isAdmin ? `${urlPath}/products/all` : `${urlPath}/products`;
        const headers = user.isAdmin ? {Authorization: `Bearer ${localStorage.getItem('token')}`} : {Authorization: 'none'};

        fetch(url,{headers: headers})
            .then(res => res.json())
            .then(data => {

                let filterData = location.state !== null ? 
                    data.filter((d) => d.name.toLowerCase().includes(location.state.filterProp)) 
                    : data;

                setProducts(filterData.map(product => {
                        
                        return (
                        <Col xs={12} md={4} className="mr-2 mb-4" key={product._id}>
                            <ProductCard  productData={product} />
                        </Col>
                    )
                }))


            })


    },[location.state, user.isAdmin])

    return(

        <Row>
            
                {products}
            
        </Row>
    )


}

export default ViewAll


