import { useContext, useState, useEffect } from "react";
import { Button, Card } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPenToSquare, faArchive } from '@fortawesome/free-solid-svg-icons'
import Swal from "sweetalert2";

import UserContext from "../../../UserContext";
import CartButtons from "./CartButtons";

function ProductCard({ productData }) {

    const urlPath = process.env.REACT_APP_API_URL;
    let token = localStorage.getItem('token');

    const { user } = useContext(UserContext)
    const { name, price, imagePath, _id, isActive } = productData
    const [archiveIcon, setArchiveIcon] = useState('')
    const [isActiveVal, setIsActiveVal] = useState(isActive)

    const navigate = useNavigate();

    useEffect(() => {
        if (isActiveVal) {
            setArchiveIcon('text-primary')
        } else {
            setArchiveIcon('text-danger')
        }
    }, [isActiveVal, archiveIcon])

    let imageHolder = imagePath === '' ? '/images/image_holder.png' : `/${imagePath}`

    
    function archiveProduct(){
        let msgVal = isActiveVal ? 'deactivate' : 'activate';

        Swal.fire({
            title: 'Confirm?',
            text: `Are you sure you want to ${msgVal} this product?`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
          }).then((result) => {
                if (result.isConfirmed) {

                let url = `${urlPath}/products/${_id}/update`

                fetch(url,{
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${token}`
                    },
                    body: JSON.stringify({
                        isActive: !isActiveVal
                    })
                })
                .then(res => res.json())
                .then(result => {
                    setIsActiveVal(!isActiveVal)
                })


                }
          })



    }
    
    const imgStyle = {
        width: '250px',
        height: '185px',
        objectFit: 'contain'
    }


    return (
        <Card style={{ width: '18rem' }} className="mx-5 h-100">
            <Card.Body>
                <Card.Img variant="top" src={imageHolder} style={imgStyle}/>
                <Card.Title>
                    <Card.Text 
                        as={Link} 
                        to={'/product/view/' + _id} 
                        state={{productProp: productData}} 
                        style={{'textDecoration': 'none',fontSize: '1rem'}}
                        >{name}
                    </Card.Text>
                </Card.Title>
                <Card.Subtitle className="mb-2 text-muted">&#8369; {price}</Card.Subtitle>
            </Card.Body>
            <Card.Footer>
                {user.isAdmin ?
                    <>
                        <Button variant="light"
                            onClick={() => {
                                navigate(`/product/update/${_id}`, {
                                    state: {
                                        product: productData
                                    }
                                })
                            }}
                        >
                            <FontAwesomeIcon icon={faPenToSquare} size='lg' />
                        </Button>
                        <Button variant="light" onClick={() => archiveProduct()}>
                            <FontAwesomeIcon icon={faArchive} size='lg' className={archiveIcon}/>
                        </Button>
                    </>
                   
                    :
                   
                    <CartButtons cartProp={productData} />

                }

            </Card.Footer>
        </Card>
    )
}

export default ProductCard