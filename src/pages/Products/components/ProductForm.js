import { useState } from "react";
import { Image, Row, Col, Form, Button, FloatingLabel } from "react-bootstrap";

function ProductForm({ productProp }) {


    const [product, setProduct] = useState(productProp)
    const [switchState, setSwitchState] = useState(product.isActive);

    const handleChange=(e)=>{
        setProduct((product) => ({ ...product, 'isActive': !switchState }))
        setSwitchState(!switchState)
         
     }
  

    let imageHolder = productProp.imagePath === undefined ? '/images/image_holder.png' : `/${productProp.imagePath}`

    const handleSubmit = (e) => {
        e.preventDefault();
        productProp.onSubmit(product)
        if(product.mode === 'Add'){
            setProduct(productProp)
        }
    }


    function getChanges(field) {
        return (e) => {
            setProduct((product) => ({ ...product, [field]: e.target.value }))
        };
    }


    const imgStyle = {
        width: '350px',
        height: '300px',
        objectFit: 'contain'
    }

    return (
        <Form id='productForm' onSubmit={handleSubmit}>
            <h1>{product.mode} Product</h1>
            <Row>
                <Col xs={12} md={4} className="mb-3">
                    <Image src={imageHolder} style={imgStyle} thumbnail />
                </Col>
                <Col xs={12} md={8}>
                    <FloatingLabel className="mb-3" controlId="productName" label='Product Name'>
                        <Form.Control
                            type="text"
                            onChange={getChanges('name')}
                            value={product.name} />
                    </FloatingLabel>
                    <FloatingLabel className="mb-3" controlId="price" label='Price (&#8369;) '>
                        <Form.Control
                            type="number"
                            onChange={getChanges('price')}
                            value={product.price} />
                    </FloatingLabel>
                    <FloatingLabel className="mb-3" controlId="description" label='Description'>
                        <Form.Control
                            as='textarea'
                            style={{ height: '150px' }}
                            onChange={getChanges('description')}
                            value={product.description} />
                    </FloatingLabel>
                    <Form.Group as={Row} className="mb-3">
                        <Col sm={10}>
                            <Form.Check
                                type="switch"
                                id="custom-switch"
                                label="Is Active"
                                defaultChecked={switchState}
                                onChange={handleChange}
                            />
                        </Col>
                    </Form.Group>
                </Col>
            </Row>


            <Button
                variant="primary"
                type="submit"
                id='submitBtn'
                style={{ float: 'right', marginLeft: '10px' }}>{product.submitLbl}</Button>

        </Form>
    )
}

export default ProductForm;