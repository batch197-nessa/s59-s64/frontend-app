import { useContext } from "react"
import { useNavigate } from "react-router-dom"
import { Button } from "react-bootstrap"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"

import { faCartPlus, faHeart as solidHeart } from '@fortawesome/free-solid-svg-icons'
import { faHeart as regHeart } from '@fortawesome/free-regular-svg-icons'

import CartContext from "../../../CartContext"
import UserContext from "../../../UserContext"
import WishContext from "../../../WishContext"

function CartButtons({ cartProp }) {

    const { cart, setCart } = useContext(CartContext);
    const { user } = useContext(UserContext);
    const { wish, setWish } = useContext(WishContext)
    const navigate = useNavigate();
    const productId = cartProp.productId ? cartProp.productId : cartProp._id;
    const wIdx = wish.findIndex((item) => item.productId === productId);

    function addToCart() {

        if (user._id === undefined) {
            navigate('/login')
        } else {

            let cnt = 1;

            const idx = cart.findIndex((item) => item.productId === productId) //check if product already exist in cart

            let productObj = {
                productId: productId,
                productName: cartProp.name,
                price: cartProp.price,
                quantity: cnt
            }

            if (idx !== -1) { //item exists
                // cart[idx].quantity = cnt + parseInt(cart[idx].quantity)
                setCart((cart) => {
                    const arr = [...cart]
                    arr[idx].quantity = cnt + parseInt(cart[idx].quantity);
                    return arr
                })

            } else {
                setCart((cart) => [...cart, productObj])
            }
        }





        // console.log(cart)
    }

    function updateWish() {

        if (user._id === undefined) {
            navigate('/login')
        } else {

            let productObj = {
                productId: productId,
                productName: cartProp.name,
                price: cartProp.price
            }

            if (wIdx !== -1) { //item exists

                setWish((wish) => wish.filter((w) => {
                    return w.productId !== productId
                }))

            } else {

                setWish((wish) => [...wish, productObj])
            }
        }

    }

    return (
        <>
            <Button variant="light" onClick={() => updateWish()}>
                {
                    wIdx !== -1 ? 
                    <FontAwesomeIcon icon={solidHeart} size='lg' />
                    :
                    <FontAwesomeIcon icon={regHeart} size='lg' />
                }
            </Button>
            <Button
                variant="light"
                className="mx-5"
                onClick={() => addToCart()}
            >
                <FontAwesomeIcon icon={faCartPlus} size='lg' />
            </Button>
        </>
    )
}

export default CartButtons