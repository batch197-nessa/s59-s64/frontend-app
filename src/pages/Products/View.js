import { useContext, useEffect, useState } from "react";
import { Col, Image, Row } from "react-bootstrap";
import { useParams, useLocation } from "react-router-dom";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStar } from '@fortawesome/free-regular-svg-icons'

import UserContext from "../../UserContext";

import CartButtons from "./components/CartButtons";

function View() {

    const { productId } = useParams();
    const location = useLocation();
    let productDataInit = { productId: '', name: '', description: '', price: 0, imagePath: '' };
    let productProp = location.state === null ? productDataInit : location.state.productProp

    const {user} = useContext(UserContext)

    const [productData, setProductData] = useState(productProp);

    useEffect(() => {
        const urlPath = process.env.REACT_APP_API_URL;
        const url = `${urlPath}/products/${productId}`;

        if (location.state) {
           
            setProductData({
                productId: productProp._id,
                name: productProp.name,
                description: productProp.description,
                price: productProp.price,
                imagePath: productProp.imagePath
            })
        } else { //in case link did not came from products list
            fetch(url)
                .then(res => res.json())
                .then(data => {
                    console.log(data)

                    setProductData({
                        productId: data._id,
                        name: data.name,
                        description: data.description,
                        price: data.price,
                        imagePath: data.imagePath
                    })

                })
        }



    }, [])

    function StarReview() {
        return [...Array(5)].map((e, i) =>
            <FontAwesomeIcon key={i} icon={faStar} size='sm' />
        )
    }

    let imageHolder = productData.imagePath === '' ? '/images/image_holder.png' : `/${productData.imagePath}`

    const imgStyle = {
        width: '346px',
        height: '252px',
        objectFit: 'contain'
    }




    return (

        <>
            <Row>
                <Col xs={12} md={4}>
                    <Image src={imageHolder} thumbnail style={imgStyle} />
                </Col>
                <Col xs={12} md={8}>
                    <h4 className="my-5">{productData.name}</h4>
                    <h4 className="my-5">&#8369; {productData.price}</h4>
                    {
                        !user.isAdmin && 
                        <CartButtons cartProp={productData} />

                    }
                </Col>
            </Row>
            <Row className="mt-5">
                <Col xs={12}>
                    <h5>Description</h5>
                    <hr />
                    <article>
                        {productData.description}
                    </article>
                </Col>
            </Row>
            <Row className="mt-5">
                <Col xs={12}>
                    <h5>Reviews
                        <StarReview />
                    </h5>
                    <hr />
                    <article>
                    </article>
                </Col>
            </Row>


        </>

    )
}

export default View;