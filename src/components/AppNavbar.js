import { useContext, useEffect, useState } from "react";
import { Navbar, Nav, NavDropdown, Container, InputGroup, Form, Button } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
// import { regular, brands } from '@fortawesome/fontawesome-svg-core/import.macro'
import { faCartShopping, faSignOut, faMagnifyingGlass, faHeart } from '@fortawesome/free-solid-svg-icons'
import { Badge } from '@mui/material';

import './AppNavbar.css';
import UserContext from "../UserContext";
import CartContext from "../CartContext";
import WishContext from "../WishContext";

function AppNavbar() {

    const { user } = useContext(UserContext);
    const { cart } = useContext(CartContext);
    const {wish} = useContext(WishContext)
    const [cartBadge, setCartBadge] = useState(0);
    const wishBadge = wish.length;
    const [searchText, setSearchText] = useState('')
    let navigate = useNavigate()


    useEffect(() => {

        let num = cart.reduce((total, item) => item.quantity + total, 0)
        setCartBadge(num)

    }, [cart])


    return (
        <>
            <Navbar bg="dark" className="mb-4" expand='lg'>
                <Container>
                    <Navbar.Brand href="#home">
                        <img 
                            src='/images/logo_brand_ls.png'
                            width="90"
                            height="50"
                            // className="d-inline-block align-top"
                            className="d-none d-sm-none d-lg-block"
                            alt="Workacute logo"
                        />
                    </Navbar.Brand>
                    <Navbar.Brand href="#home">
                        <img
                            src='/images/logo.png'
                            width="50"
                            height="50"
                            // className="d-inline-block align-top"
                            className="d-block d-sm-block d-lg-none"
                            alt="Workacute logo"
                        />
                    </Navbar.Brand>
                   




                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav" >
                        <Nav className="me-auto">
                            <Nav.Link as={Link} to='/'>Home</Nav.Link>
                            <Nav.Link as={Link} to='/products'>Products</Nav.Link>
                            {user.isAdmin &&
                                <NavDropdown title="Admin" id="basic-nav-dropdown">
                                    <NavDropdown.Item as={Link} to='/product/add'>Add Product</NavDropdown.Item>
                                    <NavDropdown.Item as={Link} to='/orders'>
                                        View Orders
                                    </NavDropdown.Item>
                                    <NavDropdown.Item as={Link} to='/users/list'>
                                        Users
                                    </NavDropdown.Item>
                                </NavDropdown>
                            }


                        </Nav>
                    </Navbar.Collapse>
                    <InputGroup className="mb-3">
                        <Form.Control
                            placeholder="..."
                            aria-describedby="basic-addon2"
                            value={searchText}
                            onChange={(e) => setSearchText(e.target.value)}
                        />
                        <Button variant="secondary"
                            onClick={() => {
                                navigate(`/products`, {
                                    state: {
                                        filterProp: searchText
                                    }
                                })
                                setSearchText('')
                            }}
                        >
                            <FontAwesomeIcon icon={faMagnifyingGlass} size='sm' className="text-light" />

                        </Button>
                        {/* <InputGroup.Text id="basic-addon2">Search</InputGroup.Text> */}
                    </InputGroup>

                    <Navbar.Collapse id="basic-navbar-nav" >
                        <Nav className="me-auto">
                            {
                                (!user.isAdmin && user._id !== undefined) &&
                                <>
                                <Nav.Link as={Link} to='/wishlist'>
                                    <Badge badgeContent={wishBadge}
                                        color="error">
                                        <FontAwesomeIcon icon={faHeart} size='lg' />
                                    </Badge>
                                </Nav.Link>
                                <Nav.Link as={Link} to='/cart'>
                                    <Badge badgeContent={cartBadge}
                                        color="error">
                                        <FontAwesomeIcon icon={faCartShopping} size='lg' />
                                    </Badge>
                                </Nav.Link>
                                </>
                            }


                            {(user._id !== undefined) ?

                                <>
                                    <NavDropdown title={`Hi, ${user.firstName}! `} id="basic-nav-dropdown">
                                        <NavDropdown.Item as={Link} to='/user'>User Profile</NavDropdown.Item>
                                        <NavDropdown.Item as={Link} to='/orders'>
                                            Orders
                                        </NavDropdown.Item>
                                    </NavDropdown>
                                    <Nav.Link as={Link} to='/logout'>
                                        <FontAwesomeIcon icon={faSignOut} size='lg' />
                                    </Nav.Link>

                                </>
                                :
                                <>
                                    <Nav.Link as={Link} to='/login'>Login</Nav.Link>
                                    <Nav.Link as={Link} to='/register'>Register</Nav.Link>
                                </>
                            }



                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>

        </>
    )

}

export default AppNavbar;