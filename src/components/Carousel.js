import { Row, Col, Carousel, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom';
import Banner from './Banner';
import './AppCarousel.css'


function AppCarousel() {


    return (
        <Row >
            <Col className="d-block d-sm-block d-lg-none">
                <Banner />
            </Col>
            <Col className='d-none d-sm-none d-lg-block'>
                <Carousel style={{backgroundColor: 'darkseagreen'}}>
                    <Carousel.Item>
                        <img
                            className="d-block w-100" 
                            src='images/pexels-jess-bailey-designs-788946.jpg' 
                            alt="First slide"
                        />
                        <Carousel.Caption className='caption-center'>
                            <h3 style={{color: '#4c5652'}} >Get the latest iPhone</h3>
                            <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                            <Button variant='primary' as={Link} to='/login'>Pre-Order Now!</Button>
                        </Carousel.Caption>
                    </Carousel.Item>
                    <Carousel.Item>
                        <img
                            className="d-block w-100"
                            src='images/pexels-noah-erickson-404280.jpg'
                            alt="Second slide"
                        />

                        <Carousel.Caption className='caption-center-right'>
                            <h3>Android Phone</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </Carousel.Caption>
                    </Carousel.Item>
                    <Carousel.Item>
                        <img
                            className="d-block w-100"
                            src='images/pexels-lisa-fotios-1957478.jpg'
                            alt="Third slide"
                        />

                        <Carousel.Caption className='caption-top'>
                            <h3 style={{color: '#4c5652'}}>Home Office Collection</h3>
                            <p style={{color: '#4c5652'}}>
                                Praesent commodo cursus magna, vel scelerisque nisl consectetur.
                            </p>
                        </Carousel.Caption>
                    </Carousel.Item>
                </Carousel>
            </Col>
        </Row>
    )
}

export default AppCarousel;