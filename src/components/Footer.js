import { Container, Row, Col } from "react-bootstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCcVisa, faCcMastercard, faCcPaypal, faCcStripe, faFedex, faUps, faSquareFacebook, faInstagram } from '@fortawesome/free-brands-svg-icons';
import './Footer.css'

function Footer() {

    return (
        <Container fluid className="bg-dark footer">
            <Row className="">
                <Col className="text-white mb-3" xs={12} md={1}>
                    <img src='/images/logo_brand_ls.png' 
                        width='90'
                        height='60' 
                        alt="logo brand"
                    />

                </Col>

                <Col className="text-white mb-3" xs={3}>
                        <h6>Payment Methods</h6>
                        <FontAwesomeIcon icon={faCcVisa} size='lg' />
                        <FontAwesomeIcon icon={faCcMastercard} size='lg' />
                        <FontAwesomeIcon icon={faCcPaypal} size='lg' />
                        <FontAwesomeIcon icon={faCcStripe} size='lg' />
                    </Col>

                    <Col className="text-white mb-3" xs={3}>
                        <h6>Delivery Services</h6>
                        <FontAwesomeIcon icon={faFedex} size='lg' />
                        <FontAwesomeIcon icon={faUps} size='lg' />
                    </Col>

                    <Col className="text-white mb-3" xs={2}>
                        <h6>Follow Us!</h6>
                        <FontAwesomeIcon icon={faSquareFacebook} size='lg' />
                        <FontAwesomeIcon icon={faInstagram} size='lg' />
                    </Col>

                    <Col className="text-white" xs={12} md={3}>

                        <div className="small">
                            <h6>Contact Us!</h6>
                            <p className="m-0">(6328)880-0808</p>
                            <p className="m-0">helpdesk@workacute.com</p>
                            <p className="m-0">3389 Black Oak Hollow Road, Pampanga, Philippines</p>
                        </div>

                    </Col>



            </Row>
        </Container>


    )

}

export default Footer;