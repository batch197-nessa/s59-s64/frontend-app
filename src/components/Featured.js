import { Row, Col, Card } from 'react-bootstrap'
import { Link } from 'react-router-dom'


function Featured() {

    const products = [
        {
            id: '634ba06ba21dc4755451805f',
            title: 'iWatch Elegant',
            price: 40000,
            imagePath: "/images/pexels-vishven-solanki-2861929.jpg"
        },
        {
            id: '6342cb0d39c285a10765d5f9',
            title: 'Wooden Gadget Holder',
            price: 241,
            imagePath: "/images/pexels-ekaterina-bolovtsova-6373026.jpg",

        },
        {
            id: '634448c02b3c90ec364a3ffa',
            title: 'Desk Lamp',
            price: 350,
            imagePath: "/images/pexels-elvis-2528116.jpg",
        }
    ]

    return (

        <Row>
            {
                products.map((product) => {
                    return(<Col xs={12} md={4} key={product.id}>
                        <Card style={{ width: '18rem', height: '100%' }} className='mx-auto'>
                            <Card.Body>
                                <Card.Img variant="top"
                                    src={product.imagePath}
                                    width='254'
                                    height='381' 
                                    alt='product_image'
                                />
                                <Card.Title
                                    as={Link}
                                    to={'/product/view/' + product.id} 
                                    style={{textDecoration: 'none'}}
                                >
                                    {product.title}
                                </Card.Title>
                                <Card.Subtitle className="mb-2 text-muted">&#8369; {product.price}</Card.Subtitle>
                            </Card.Body>
                        </Card>
                    </Col>)
                })



            }
        </Row>
    )
}

export default Featured;