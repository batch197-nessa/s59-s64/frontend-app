import React from "react";

const WishContext = React.createContext();

export const WishProvider = WishContext.Provider;

export default WishContext;