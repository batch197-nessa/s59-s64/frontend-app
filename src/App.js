
import { useEffect, useState } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Routes, Route, Redirect, Navigate, useNavigate } from 'react-router-dom';


import AppNavbar from './components/AppNavbar';
import Footer from './components/Footer';
import Home from './pages/Home';
import Login from './pages/Auth/Login';
import Register from './pages/Auth/Register';
import Logout from './pages/Auth/Logout';
import View from './pages/Products/View';
import ViewAll from './pages/Products/ViewAll';
import ProductCreate from './pages/Products/Create';
import ProductUpdate from './pages/Products/Update';
import Cart from './pages/Orders/Cart';
import Orders from './pages/Orders/Orders';
import Wishlist from './pages/Orders/Wishlist';

import './App.css';
import { UserProvider } from './UserContext';
import { CartProvider } from './CartContext';
import { WishProvider } from './WishContext';

import Profile from './pages/Users/Profile';
import List from './pages/Users/List';
import ErrorPage from './pages/ErrorPage';

function App() {

  const [user, setUser] = useState({ _id: null, isAdmin: null });
  const [cart, setCart] = useState([]);
  const [wish, setWish] = useState([]);

  const unsetUser = () => {
    localStorage.clear();
  }

  let token = localStorage.getItem('token');

  useEffect(() => {
    const urlPath = process.env.REACT_APP_API_URL;

    let url = `${urlPath}/users/details`

    if (token) {
      fetch(url, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      }).then(res => res.json())
        .then(data => {
          // console.log(data);

          if (typeof data !== 'undefined') {
            setUser(data)
          } else {
            setUser({})
          }


        })
    } else {
      setUser({})
    }

  }, [token])




  return (

    <UserProvider value={{ user, setUser, unsetUser }}>
      <CartProvider value={{ cart, setCart }}>
        <WishProvider value={{ wish, setWish }}>
          <Router>
            <AppNavbar />
            <Container>
              <Routes>
                <Route path='/' element={<Home />} />
                <Route path='/logout' element={<Logout />} />

                { !token &&
                  <>
                    <Route path='/login' element={<Login />} />
                    <Route path='/register' element={<Register />} />
                  </>
                }
                {
                  user.isAdmin &&
                  <>
                    <Route path='/product/add' element={<ProductCreate />} />
                    <Route path='/users/list' element={<List />} />
                    <Route path='/product/update/:productId' element={<ProductUpdate />} />
                  </>
                }
                <Route path='/products' element={<ViewAll />} />
                <Route path='/product/view/:productId' element={<View />} />
                {
                  token &&
                  <>
                    <Route path='/cart' element={<Cart />} />
                    <Route path='/wishlist' element={<Wishlist />} />
                    <Route path='/orders' element={<Orders />} />
                    <Route path='/user' element={<Profile />} />
                  </>
                }

                <Route path="*" element={<ErrorPage />} />
              </Routes>
            </Container>
            <Footer />
          </Router>
        </WishProvider>
      </CartProvider>
    </UserProvider>

  );
}

export default App;
